import chai, { expect } from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import SpotifyNodeApiWrapper from '../src/index';

chai.use(sinonChai);

global.fetch = require('node-fetch');

describe('SpotifyLibrary', () => {
  it('should create an instance od SpotifyWrapper', () => {
    const spotify = new SpotifyNodeApiWrapper({});
    expect(spotify).to.be.an.instanceof(SpotifyNodeApiWrapper);
  });
});
